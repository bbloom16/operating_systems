//brian bloom
#include<iostream>
#include<sstream>
#include<fstream>
#include<string>
#include<sys/types.h>
#include<stdint.h>
#include<string>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<ctime>
using namespace std;

double cylinder[1000][1000];
int numFlops = 0;
void printCylinder(int H, int C){
  cout<<"\n-----Cylinder-----\n";
  for(int i=0; i<H; i++){
    for(int j=0; j<C; j++){
      cout<<cylinder[i][j]<<" ";
    }//end inner loop
    cout<<"\n";
  }//end outer loop
}//end printCylinder

bool checkDelta(double first, double second, double delta){
  double compare;
  if(first>second){
    compare = first-second;  
  }//end if
  else if(second>first){
    compare = second-first;
  }//end else if
  if(compare<=delta){
    //cout<<"converged on delta"<<endl;
    return true;
  }
  return false;
}//end checkDelta

bool computeIterations(double c0, double c1, double c2, int H, int C, double delta){
  double D0, N0, D1, N1, N2, D2, N3, D3, X;
  double weightA, weightB, weightC;
  bool deltaCheck = true;
  double avgTemp = 0;
  double maxDelta = 0;
  for(int i=0; i<H; i++){
    for(int j=0; j<C; j++){
      //default weights for all cases
      weightA = 0;
      weightB = 0;
      weightC = 0;

      //position of current cell 'X'
      X = cylinder[i][j];
      //use 'X' to compute avg temp
      avgTemp += X;
      numFlops++;
      //compute weightA
      weightA = X*c0;
      numFlops++;
      //wrap-around section for sides
      int LESS = j-1;
      int MORE = j+1;
      if(j==0){
	LESS = C-1;
      }//end if
      if(j==(C-1)){
	MORE = 0;
      }//end if
      //end wrap-around section

      //special cases for sides
      //no row above
      if(i==0){
	//cout<<"no row above"<<endl;
	N1 = cylinder[i][LESS];
	N2 = cylinder[i][MORE];
	D2 = cylinder[i+1][LESS];    
	N3 = cylinder[i+1][j];
	D3 = cylinder[i+1][MORE];

	//computation section
	
	//weightB
	weightB = (N1+N2+N3)*c1;
	numFlops++;
	//weightC
	weightC = (D1+D2+D3)*c2;
	numFlops++;
      }//end if
      //no row below
      else if(i==(H-1)){
	//cout<<"no row below"<<endl;
	D0 = cylinder[i-1][LESS];
	N0 = cylinder[i-1][j];
	D1 = cylinder[i-1][MORE];
	N1 = cylinder[i][LESS];
	N2 = cylinder[i][MORE];

	//computation section
	
	//weightB
	weightB = (N0+N1+N2)*c1;
	numFlops++;
	//weightC
	weightC = (D0+D1+D2)*c2;
	numFlops++;
      }//end else if
      //end special cases for sides
      //default cell
      else{
	D0 = cylinder[i-1][LESS];
	N0 = cylinder[i-1][j];
	D1 = cylinder[i-1][MORE];
	N1 = cylinder[i][LESS];
	N2 = cylinder[i][MORE];
	D2 = cylinder[i+1][LESS];    
	N3 = cylinder[i+1][j];
	D3 = cylinder[i+1][MORE];

	//computation section
	
	//weightB
	weightB = (N0+N1+N2+N3)*c1;
	numFlops++;
	//weightC
	weightC = (D0+D1+D2+D3)*c2;
	numFlops++;
      }//end else
      //end default cell

      //new value computaion of cell 'X'
      //cout<<"weightA:"<<weightA<<";weightB:"<<weightB<<";weightC:"<<weightC<<endl;
      double newValue =(weightA+weightB+weightC)/3;
      numFlops++;
      cylinder[i][j] = newValue;
      //check if this delta is the largest in this iteration
      double tempCompare = fabs(X-newValue);
      if(tempCompare>maxDelta){
	maxDelta = tempCompare;
      }
      if(checkDelta(X, newValue, delta)==false){
	//cout<<"setting detlaCheck to false"<<endl;
	deltaCheck=false;
      }
    }//end inner for
  }//end outer for
  avgTemp = avgTemp / (H*C);
  numFlops++;
  cout<<"average temp for this iteration: "<<avgTemp<<endl;
  cout<<"max delta for this iteration: "<<maxDelta<<endl;
  if(deltaCheck==true){
    cout<<"cylinder has reached convergence"<<endl;
  }//convergence check
  return deltaCheck;
}//end computeIterations

//main thread
int main(int argc, char*argv[]){
  //command line arguments:
  //H, C, tinit, c0, c1, c2, delta, max
  int H, C, tinit, max;
  double c0, c1,c2, delta;
  if(argc!=9){
    cout<<"error, invalid arguments"<<endl;
    return 0;
  }
  istringstream hss( argv[1] );
  hss >> H;
  istringstream css( argv[2] );
  css >> C;
  istringstream tinitss( argv[3] );
  tinitss >> tinit;
  istringstream c0ss( argv[4] );
  c0ss >> c0;
  istringstream c1ss( argv[5] );
  c1ss >> c1;
  istringstream c2ss( argv[6] );
  c2ss >> c2;
  istringstream deltass( argv[7] );
  deltass >> delta;
  istringstream maxss( argv[8] );
  maxss >> max;

  //output results to file
  freopen ("results.txt", "w", stdout);
  //print arguments:
  cout<<"H:"<<H<<"; C:"<<C<<"; tinit:"<<tinit<<"; c0:"<<c0<<"; c1:"<<c1<<"; c2:"<<c2<<"; delta:"<<delta<<"; max:"<<max<<endl;
  
  //make cylinder HxC array of cells
  //all temps initialized to tinit
  for(int i=0; i<H; i++){
    for(int j=0; j<C; j++){
      cylinder[i][j] = tinit;
    }//end inner for
  }//end outer for
  
  //select random cells to varying temps
  //EDIT: hardcoded for now
  int testTemp = tinit+50;
  for(int k=0; k<5; k++){
    for(int l=0; l<5; l++){
      cylinder[k][l] = testTemp;
    }//end inner for
  }//end outer for

  clock_t c_start = std::clock();
  //print initial cylinder
  printCylinder(H,C);
  bool checkIt = false;
  int IT = 0;
  while(IT<max && (checkIt==false)){
    checkIt = computeIterations(c0, c1, c2, H, C, delta);
    if(checkIt==false){
      printCylinder(H,C);
    }//end if
    IT++;
  }//end iterations
  clock_t c_end = std::clock();
  double time_elapsed_ms = 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC;
  cout << "CPU time used: " << time_elapsed_ms << " ms\n";
  cout<<"Number of FLOPS: "<<numFlops<<endl;
}//end main
