This program can be executed by entering the following types of lines into the command line:
./main 10 10 20 .5 .1 .025 .001 10000
./main 100 100 20 .5 .1 .025 .001 10000
./main 1000 1000 20 .5 .1 .025 .001 10000
This program can be recompiled by the following commands:
'make clean' then 'make'
It outputs all data to a file results.txt
