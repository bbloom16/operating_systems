//brian bloom
#include<iostream>
#include<sstream>
#include<sys/types.h>
#include<stdint.h>
#include<string>
#include<queue>
#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
using namespace std;
#include<semaphore.h>

//safe queue wrapper class
template <typename E> class safeQueue{
  queue <E> queueUsed;
  sem_t semUsed;

  public:

  safeQueue(){
    sem_init(&semUsed,0,1);
  }//end constructor
  
  E pop(){
    sem_wait(&semUsed);
    //wait on empty queue
    while(queueUsed.size()==0){
      sem_wait(&semUsed);
    }
    E item = queueUsed.front();
    queueUsed.pop();
    //sem_post(&semUsed);
    return item;
  }//end pop

  void push(E pushItem){
    //sem_wait(&semUsed);
    queueUsed.push(pushItem);
    sem_post(&semUsed);
  }//end push

  E front(){
    sem_wait(&semUsed);
    while(queueUsed.size()==0){

    }
    E result = queueUsed.front();
    sem_post(&semUsed);
    return result;
  }

  int size(){
    sem_wait(&semUsed);
    int size = queueUsed.size();
    sem_post(&semUsed);
    return size;
  }
};//end class

//struct to pass args through p thread
struct thread_args{
  safeQueue <int> queueSafe;
  int totalCount;
  int n;
};//end struct

void printStruct(thread_args *structPassed){
  cout<<"print struct"<<endl;
  //cout<<"num passed:"<<structPassed->numPassed<<endl;
  cout<<"totalCount:"<<structPassed->totalCount<<endl;
  cout<<"n:"<<structPassed->n<<endl;
  safeQueue<int> copyQueue = structPassed->queueSafe;
  while(copyQueue.size()!=0){
    cout<<"current queue:"<<copyQueue.pop()<<endl;
  }
  cout<<"end print struct"<<endl;
}//end printStruct

//worker thread method which acts as a sieve and created the next thread
void *worker_thread(void* p){
  
  thread_args *args = (thread_args*)p;
  if(args->totalCount==args->n){
    cout<<"-----DONE-----"<<endl;
    exit(EXIT_FAILURE);
  }
  
  if(args->queueSafe.size()!=0){
    int workingNum = args->queueSafe.front();
    cout<<"Prime:"<<workingNum<<endl;
    //cout<<"totalCount:"<<args->totalCount<<".n:"<<args->n<<endl;
  
    //call next thread
    thread_args argsNext;
    argsNext.totalCount = args->totalCount+1;
    argsNext.n = args->n;
  
    pthread_t tid;
    pthread_create(&tid, NULL, worker_thread, &argsNext);

    //continue to pass elements from previous queue onto the next queue
    int qElement;
    while(true){
      qElement = args->queueSafe.front();
      if(qElement%workingNum!=0){
	argsNext.queueSafe.push(qElement);
      }
      args->queueSafe.pop();
    }//end while
  }
}//end worker_thread

//main thread
int main(){
  
  cout<<"Value for number of desired primes (>2): ";
  int n;
  cin >> n;

  //send all arguments to the struct
  thread_args args;
  args.totalCount = 0;
  args.n = n;

  //call first thread
  pthread_t tid;
  int i = 2;
  pthread_create(&tid, NULL, worker_thread, &args);

  while(true){
    args.queueSafe.push(i);
    i++;
  }

}//end main
