Part A can be run by typing in ./main into the command line.
If for any reason you need to recompile the main, you can do so by executing 'make clean' then 'make'.
This program may need to be executed more than once to achieve the desired outcome.
