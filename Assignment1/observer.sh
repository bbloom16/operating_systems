#!/bin/bash
#Generate hardware stats
#NOTE: run as sudo ./observer.sh
#The meanings of the columns are as follows, from left to right:

#user: normal processes executing in user mode
#nice: niced processes executing in user mode
#system: processes executing in kernel mode
#idle: twiddling thumbs
#iowait: waiting for I/O to complete
#irq: servicing interrupts
#softirq: servicing softirqs
#The "intr" line gives counts of interrupts serviced since boot time, for each
#of the possible system interrupts. The first column is the total of all interrupts serviced; each subsequent column is the total for that particular interrupt.


echo "--------HARDWARE STATS--------"
lshw | more

echo "--------System uptime:--------"
uptime

echo "--------Kernel Version:-------"
uname -r

echo "--------CPU PROCESSES:--------"
cat /proc/stat

echo "--------Memory Info:----------"
cat /proc/meminfo

echo "--------Swaps:----------------"
cat /proc/swaps
free -h
