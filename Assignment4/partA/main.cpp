//brian bloom
#include<iostream>
#include<sstream>
#include<sys/types.h>
#include<stdint.h>
#include<string>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<ctime>
#include<pthread.h>
#include<unistd.h>
using namespace std;
#include<semaphore.h>

sem_t studentA, studentB, studentC, teacher;

struct thread_args{
  string name;
};//end struct

void *a_thread(void* p){
  while(true){
  sem_wait(&studentA);
  thread_args * args = (thread_args*)p;
  string name = args->name;
  cout<<name<<" is eating."<<endl;
  //wait a random amount of time
  int sleepyBoi = rand() % 3;
  sleep(sleepyBoi);
  sem_post(&teacher);
  }
}//end worker_thread

void *b_thread(void* p){
  while(true){
  sem_wait(&studentB);
  thread_args * args = (thread_args*)p;
  string name = args->name;
  cout<<name<<" is eating."<<endl;
  //wait a random amount of time
  int sleepyBoi = rand() % 3;
  sleep(sleepyBoi);
  sem_post(&teacher);
  }
}//end worker_thread

void *c_thread(void* p){
  while(true){
  sem_wait(&studentC);
  thread_args * args = (thread_args*)p;
  string name = args->name;
  cout<<name<<" is eating."<<endl;
  //wait a random amount of time
  int sleepyBoi = rand() % 3;
  sleep(sleepyBoi);
  sem_post(&teacher);
  }
}//end worker_thread

int main(){
  //initialize the rascals
  pthread_t spanky;
  pthread_t alf;
  pthread_t darla;
  
  thread_args spankyArgs;
  spankyArgs.name = "Spanky";
  thread_args alfArgs;
  alfArgs.name = "Alf";
  thread_args darlaArgs;
  darlaArgs.name = "Darla";
  
  //sem_t studentA;
  sem_init(&studentA, 0, 0);
  //sem_t studentB;
  sem_init(&studentB, 0, 0);
  //sem_t studentC;
  sem_init(&studentC, 0, 0);
  //sem_t teacher;
  sem_init(&teacher, 0, 1);

  pthread_create(&spanky, NULL, a_thread, &spankyArgs);
  pthread_create(&alf, NULL, b_thread, &alfArgs);
  pthread_create(&darla, NULL, c_thread, &darlaArgs);

  
  while(true){
    sem_wait(&teacher);
    int pick = rand() % 3;
    cout<<"Pick is "<<pick<<endl;
    string putDown;
    if(pick==0){
      sem_post(&studentA);
    }
    else if(pick==1){
      sem_post(&studentB);
    }
    else{
      sem_post(&studentC);
    }
  }//end for

  
  
}//end main
