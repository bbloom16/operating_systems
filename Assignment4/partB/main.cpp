//brian bloom
#include<iostream>
#include<sstream>
#include<sys/types.h>
#include<stdint.h>
#include<string>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<ctime>
#include<pthread.h>
#include<unistd.h>
#include<semaphore.h>
using namespace std;


typedef struct{
  sem_t writerOP;
  int num;
}thread_args;//end struct

thread_args t;

sem_t db;
sem_t rc_mutex;
int rc;

void readDB(){
  cout<<"reading database"<<endl;
  //sleep(.1);
}//end readDB

void writeDB(){
  cout<<"writing database"<<endl;
  cout<<"rc: "<<rc<<endl;
  sleep(5);
}//end writeDB

void useData(){
  cout<<"Using Data"<<endl;
  //sleep(0);
}//end useData

void processData(){
  cout<<"Processing Data"<<endl;
  sleep(5);
}//end processData

void* reader(void * p)
{
  thread_args * args = (thread_args*)p;
  int id = args->num;
  //cout<<"Thread: "<<id<<endl;
  while (true)
   {
      sem_wait(&rc_mutex);
         rc++;
         if (rc==1) sem_wait(&db);
      sem_post(&rc_mutex);

      readDB();

      sem_wait(&rc_mutex);
         rc--;
         if (rc==0) sem_post(&db);
	 sem_post(&rc_mutex);

      useData();
   }
}

void * writer(void *)
{
  while(true)
   {
     //sem_wait(&t.writerOP);
     processData();
      sem_wait(&db);
         writeDB();
      sem_post(&db);
      //sem_post(&t.writerOP);
    }
}

int main(){
  cout<<"Enter the number of readers (N): ";
  int n;
  cin >> n;

  rc = 0;
  sem_init(&db, 0, 1);
  sem_init(&rc_mutex, 0, 1);
  sem_init(&t.writerOP, 0, 1);
  
  pthread_t readers[n];
  pthread_t writerThread;

  pthread_create(&writerThread, NULL, writer, NULL);
  
  //make reader threads
  for(int i=0; i<n; i++){
    thread_args tidArgs;
    tidArgs.num = i;
    //cout<<tidArgs.num<<endl;
    pthread_create(&readers[i], NULL, reader, &tidArgs);
    pthread_join(readers[i], NULL);
  }

    
}//end main

