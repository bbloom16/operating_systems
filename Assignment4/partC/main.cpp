//brian bloom
#include<iostream>
#include<sstream>
#include<sys/types.h>
#include<stdint.h>
#include<string>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<ctime>
#include<pthread.h>
#include<unistd.h>
#include<semaphore.h>
#include<queue>
using namespace std;

//safe queue wrapper class
template <typename E> class safeQueue{
  queue <E> queueUsed;
  sem_t semUsed;

public:

  safeQueue(){
    sem_init(&semUsed,0,1);
  }//end constructor

  E pop(){
    sem_wait(&semUsed);
    //wait on empty queue
    while(queueUsed.size()==0){
      sem_wait(&semUsed);
    }
    E item = queueUsed.front();
    queueUsed.pop();
    sem_post(&semUsed);
    return item;
  }//end pop

  void push(E pushItem){
    sem_wait(&semUsed);
    queueUsed.push(pushItem);
    sem_post(&semUsed);
  }//end push

  E front(){
    sem_wait(&semUsed);
    while(queueUsed.size()==0){

    }
    E result = queueUsed.front();
    sem_post(&semUsed);
    return result;
  }

  int size(){
    sem_wait(&semUsed);
    int size = queueUsed.size();
    sem_post(&semUsed);
    return size;
  }
};//end class   

struct thread_args{
  int id;
};//end struct

safeQueue <int> patientQ;

sem_t docWorking;
sem_t patientCounter;
sem_t patientCalled[13];
void * doctor(void *){
  while(true)
  {
   
    if(patientQ.size()==0){
      cout<<"Doctor is sleeping. No patients"<<endl;
    }
    else{
      int workingPatient = patientQ.front();
      cout<<"Doctor is with patient "<<workingPatient<<endl;
      patientQ.pop();
      sem_post(&patientCalled[workingPatient]);
    } 
  }//end while
}//end doctor

void * patient(void* p){
  thread_args * args = (thread_args*)p;
  int patientID = args->id;
  cout<<"pushing "<<patientID<<endl;
  
  while(true){
    patientQ.push(patientID);
    sem_wait(&patientCalled[patientID]);
    if(patientQ.size()<10){
      cout<<"patient "<<patientID<<" going to waiting room"<<endl;
    }
    else{
      cout<<"patient "<<patientID<<" leaving"<<endl;
    }
  }//end while
}//end patient

int main(){
  sem_init(&docWorking, 0, 1);
  sem_init(&patientCounter, 0, 1);
 
  //doctor thread
  pthread_t doctorThread, patientThread;
  pthread_create(&doctorThread, NULL, doctor, NULL);
 
  //push 13 patients
  for(int i=0; i<13; i++){    
    sem_init(&patientCalled[i], 0, 0);
    thread_args *targs = new thread_args;
    targs->id = i;
    pthread_create(new pthread_t(), NULL, patient, targs);
  }//end for

  while(true);
}//end main


