#include<iostream>
#include<stdio.h>
#include<string>
#include<queue>
#include<deque>
using namespace std;

void printQueue(queue<char> queuePassed){
  queue<char> tempQ = queuePassed;
  while(!tempQ.empty()){
    char element = tempQ.front();
    cout<<element<<endl;
    tempQ.pop();
  }//end while
}//end printQueue

void printDeque(deque<char> dequePassed){
  deque<char> tempQ = dequePassed;
  while(!tempQ.empty()){
    char element = tempQ.front();
    cout<<element<<endl;
    tempQ.pop_front();
  }//end while
}//end printDeque

bool pageFault(queue<char> queuePassed, char nextNum){
  queue<char> tempQ = queuePassed;
  while(!tempQ.empty()){
    char element = tempQ.front();
    if(element==nextNum){
      return false;
    }
    tempQ.pop();
  }//end while
  return true;
}//end pageFault

bool pageFault(deque<char> queuePassed, char nextNum){
  deque<char> tempQ = queuePassed;
  while(!tempQ.empty()){
    char element = tempQ.front();
    if(element==nextNum){
      return false;
    }
    tempQ.pop_front();
  }//end while
  return true;
}//end pageFault

deque<char> optimalPop(deque<char> qPassed, string faultList, int currentPos){
  deque<char> tempQ = qPassed;
  int i=currentPos;
  while(i<faultList.length() && tempQ.size()>1){
    if(faultList[i]!=' '){
      
      //iterate through temp queue, if matches, pop off until one left on queue (that will be one to come off)
       for(auto it = tempQ.begin(); it!=tempQ.end(); ){
	 if(*it==faultList[i] && tempQ.size()>1){
	   it = tempQ.erase(it);
	 }
	 else{
	   ++it;
	 }
      }//end for
    }//end if
    i++;
  }//end while

  //last on queue will be the one to pop
  char result = tempQ.front();
  cout<<"result: "<<result<<endl;
  for(auto it = qPassed.begin(); it!=qPassed.end(); ){
    if(*it==result){
      it = qPassed.erase(it);
    }
    else{
      ++it;
    }
  }
  
  return qPassed;
}//end optimalPop

int optimal(string faultList){
  cout<<"=========OPTIMAL=========="<<endl;
  //max four page frame
  deque<char> optimalQ;
  int numPageFaults = 0;
  for(int i=0; i<faultList.length(); i++){
    //ignore spaces
    if(faultList[i]!=' '){
      //check if page fault occurs
      bool pf = pageFault(optimalQ, faultList[i]);
      //if it does, add the new number to the queue
      if(pf==true){
	//if room on the queue add number to top of queue
	if(optimalQ.size()<4){
	  optimalQ.push_back(faultList[i]);
	}
	//if no extra room, remove the entry that will not be used for the longest time in the future
	else{
	  //method to pop the optimal entry
	  optimalQ = optimalPop(optimalQ, faultList, i);
	  //push the new entry onto the back of the queue
	  optimalQ.push_back(faultList[i]);
	}
      }
      //output queue and check queue for page fault
      cout<<"=====print queue====="<<endl;
      printDeque(optimalQ);
      if(pf==true){
	//add to total number of faults
	numPageFaults++;
	cout<<"Page Fault"<<endl;
      }
    }
  }//end for

  cout<<"Number of total optimal faults: "<<numPageFaults<<endl;
  return numPageFaults;
}//end optimal

int fifo(string faultList){
  cout<<"=========FIFO=========="<<endl;
  //max four page frame
  queue<char> fifoQ;
  int numPageFaults = 0;
  for(int i=0; i<faultList.length(); i++){
    //ignore spaces
    if(faultList[i]!=' '){
      //check if page fault occurs
      bool pf = pageFault(fifoQ, faultList[i]);
      //if it does, add the new number to the queue
      if(pf==true){
	//if room on the queue add number
	if(fifoQ.size()<4){
	  fifoQ.push(faultList[i]);
	}
	//if no extra room, remove the oldest entry
	else{
	  fifoQ.pop();
	  fifoQ.push(faultList[i]);
	}
      }
      //output queue and check queue for page fault
      cout<<"=====print queue====="<<endl;
      printQueue(fifoQ);
      if(pf==true){
	//add to total number of faults
	numPageFaults++;
	cout<<"Page Fault"<<endl;
      }
    }
    
  }//end for

  cout<<"Number of total fifo faults: "<<numPageFaults<<endl;
  return numPageFaults;
}//end fifo

int lru(string faultList){
  cout<<"=========LRU=========="<<endl;
  //max four page frame
  deque<char> lruQ;
  int numPageFaults = 0;
  for(int i=0; i<faultList.length(); i++){
    //ignore spaces
    if(faultList[i]!=' '){
      //check for page fault
      bool pf = pageFault(lruQ, faultList[i]);
      //if it does, put number on top of queue
      if(pf==true){
	//if queue not full yet, put on the top
	if(lruQ.size()<4){
	  lruQ.push_front(faultList[i]);
	}
	//if queue full, take off the least recently used one and put most recently used on top
	else{
	  lruQ.pop_back();
	  lruQ.push_front(faultList[i]);
	}
      }//end if
      //if no page fault, re order current queue to reflect most recently used
      else{
	/*while(lruQ.front()!=faultList[i]){
	  char temp = lruQ.front();
	  lruQ.pop_front();
	  lruQ.push_back(temp);
	  }//end while*/
      }//end else
      //output queue and check queue for page fault
      cout<<"=====print queue====="<<endl;
      printDeque(lruQ);
      if(pf==true){
	numPageFaults++;
	cout<<"Page Fault"<<endl;
      }
    }    
  }//end for

  cout<<"Number of total lru faults: "<<numPageFaults<<endl;
  return numPageFaults;
}//end lru

void compare(int faultsOptimal, int faultsFifo, int faultsLru){
  if(faultsOptimal<faultsFifo && faultsOptimal<faultsLru){
    cout<<"optimal"<<endl;
  }
  else if(faultsFifo<faultsOptimal && faultsFifo<faultsLru){
    cout<<"fifo"<<endl;
  }
  else if(faultsLru<faultsOptimal && faultsFifo>faultsLru){
    cout<<"lru"<<endl;
  }
  else{
    cout<<"tie"<<endl;
  }
}//end compare

int main(){
  string faultList = "0 2 1 3 5 4 6 3 7 4 7 3 3 5 5 3 1 1 1 7 2 3 4 1";

  int faultsOptimal = optimal(faultList);
  int faultsFifo = fifo(faultList);
  int faultsLru = lru(faultList);
  
  cout<<"the most effective alg is: ";
  compare(faultsOptimal, faultsFifo, faultsLru);
}//end main
