//brian bloom
#include<iostream>
#include<fstream>
#include<sstream>
#include<sys/types.h>
#include<stdint.h>
#include<string>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<ctime>
#include<pthread.h>
#include<unistd.h>
#include<bits/stdc++.h>
using namespace std;
#include<semaphore.h>

class pointClass{
public:
  int IDtag;
  char first;
  bool direc;
  char second;

  void printPointer(){
    string arrow;
    if(direc==false){
      arrow = "rel";
    }
    else{
      arrow = "req";
    }
    
    cout<<first<<" "<<arrow<<" "<<second<<endl;
  }//end printPointer
};//end pointClass

//2D vector of categories (A, B, C, etc.)
vector<vector<pointClass> > categories;
//global tag for each model process (used for checking string permutations)
int ID;
//vector of used permutations
vector<string> usedPerms;

void interpret(vector<char> permPassed){
  vector<int> iterateCount;

  for(int i=0; i<categories.size(); i++){
    iterateCount.push_back(0);
  }

  //iterate through permutation passed to generate order
  for(auto it = permPassed.begin(); it!=permPassed.end(); ++ it){
    if(*it=='A'){
      auto tempIt = categories.at(0).begin();
      tempIt[iterateCount[0]].printPointer();
      iterateCount[0] = iterateCount[0] + 1;
    }
    else if(*it=='B'){
      auto tempIt = categories.at(1).begin();
      tempIt[iterateCount[1]].printPointer();
      iterateCount[1] = iterateCount[1] + 1;
      //it2->printPointer();
      //it2++;
    }
    else if(*it=='C'){
      auto tempIt = categories.at(2).begin();
      tempIt[iterateCount[2]].printPointer();
      iterateCount[2] = iterateCount[2] + 1;
    }
    else if(*it=='D'){
      auto tempIt = categories.at(3).begin();
      tempIt[iterateCount[3]].printPointer();
      iterateCount[3] = iterateCount[3] + 1;
    }
    else if(*it=='E'){
      auto tempIt = categories.at(4).begin();
      tempIt[iterateCount[4]].printPointer();
      iterateCount[4] = iterateCount[4] + 1;
    }
    else if(*it=='F'){
      auto tempIt = categories.at(5).begin();
      tempIt[iterateCount[5]].printPointer();
      iterateCount[5] = iterateCount[5] + 1;
    }
    else if(*it=='G'){
      auto tempIt = categories.at(6).begin();
      tempIt[iterateCount[6]].printPointer();
      iterateCount[6] = iterateCount[6] + 1;
    }
      
  }//end for
  
}//end interpret

//method to generate permutations
void makeCombos(){
  vector<char> firstPerm;
  //make array based off size of each category
  for(auto it = categories.begin(); it!=categories.end(); ++it){
    char tempAdd = it->begin()->first;
    //cout<<"tempAdd: "<<tempAdd<<endl;
    int tempLength = it->size();
    //cout<<"tempLength: "<<tempLength<<endl;

    for(int i=0; i<tempLength; i++){
      firstPerm.push_back(tempAdd);
    }//end for
  }//end for

  //generate all permutations
  int countAllPerms = 0;
  do{
    for(int i=0; i<firstPerm.size(); i++){
      cout<<firstPerm[i];
    }//end for    
    cout<<endl;

    interpret(firstPerm);
    countAllPerms++;
  }while(next_permutation(firstPerm.begin(), firstPerm.end()));

  cout<<"Total Perms: "<<countAllPerms<<endl;
}//end makePermutations

//method to parse the file
void parseFile(string fileName){  
  ifstream file;
  file.open(fileName.c_str());

  //copy file data to string
  string content((istreambuf_iterator<char>(file)), (istreambuf_iterator<char>()));
  cout<<"-----start file-----"<<endl;
  cout<<content<<endl;
  cout<<"-----end file-----"<<endl;

  //split string into tokens of three
  istringstream iss(content);
//take each token
    string two;
    char one, three;
  while(iss >> one >> two >> three){
    
    
    /*
    iss>>one;
    iss>>two;
    iss>>three;
    */
    cout<<"Symbols: "<<one<<" - "<<two<<" - "<<three<<endl;
    /*if(two==''){
      cout<<"hit"<<endl;
    }*/
    //make into one object
    pointClass tempAdd;
    tempAdd.IDtag = ID;
    tempAdd.first = one;
    if(two.compare("req")==0){
	tempAdd.direc = true;
    }
    else{
      tempAdd.direc = false;
    }
    tempAdd.second = three;
    
    //if empty, add category and add that item to the new category
    if(categories.empty()){
      cout<<"one"<<endl;
      vector<pointClass> newItem;
      newItem.push_back(tempAdd);
      categories.push_back(newItem);
    }//end if
    
    //if category does exit, push new item to that category
    else if(categories.back().back().first==one){
      cout<<"two"<<endl;
      categories.back().push_back(tempAdd);
    }//end else if

    //if category does not exist, make new category and add that item to new category
    else{
      cout<<"three"<<endl;
      vector<pointClass> newItem;
      newItem.push_back(tempAdd);
      categories.push_back(newItem);
    }//end else

    ID++;
  }//end while
  
}//end parseFile

int main(){
  cout<<"Enter file name: ";
  string fileName;
  cin>>fileName;

  ID = 0;
  
  parseFile(fileName);
  makeCombos();
}//end main

/*  
void addEdge(vector<char> adj[], int u, int v){
  adj[u].push_back(v);
  adj[v].push_back(u);
  
}//end addEdge

void printGraph(vector<char> adj[], int V){
  for (int v = 0; v < V; ++v){
    cout << "\n Adjacency list of vertex "
	 << v << "\n head ";
    for (auto x : adj[v])
      cout << "-> " << x;
    printf("\n");
  } 
}//end printGraph
*/
