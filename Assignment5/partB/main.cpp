//brian bloom
//NOTE: used https://stackoverflow.com/questions/304752/how-to-estimate-the-thread-context-switching-overhead for mutex lock/unlock implementation idea
#include <iostream>
#include <ctime>
#include <ratio>
#include <chrono>
#include <fstream>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sched.h>
#include <pthread.h>
#include <semaphore.h>
using namespace std;

sem_t toParent, toChild;

pthread_mutex_t LOCK;
pthread_mutex_t START;

pthread_cond_t CONDITION;

int counter = 0;

void * thread_call(void * p){
  pthread_mutex_lock(&START);
  pthread_mutex_unlock(&START);

  pthread_mutex_lock(&LOCK);

  if(counter > 0){
    pthread_cond_signal(&CONDITION);
  }
  while(true){
    counter++;
    pthread_cond_wait(&CONDITION, &LOCK);
    pthread_cond_signal(&CONDITION);
    
  }//end while
  pthread_mutex_unlock(&LOCK);
  
}//end thread call

int main ()
{
  //put all results in README.txt
  freopen ("README.txt", "w", stdout);

  //high resolution timer
  using namespace std::chrono;
  high_resolution_clock::time_point t1;
  high_resolution_clock::time_point t2;
  struct timespec ts;
  int ret;

  pthread_t parent, child;
  pthread_mutex_init(&LOCK, NULL);
  pthread_mutex_init(&START, NULL);
  pthread_cond_init(&CONDITION, NULL);

  pthread_mutex_lock(&START);

  pthread_create(&parent, NULL, thread_call, NULL); 
  pthread_create(&child, NULL, thread_call, NULL); 
  
  t1 = high_resolution_clock::now();
  //start the process
  pthread_mutex_unlock(&START);
  sleep(1);
  //kill the process
  pthread_mutex_lock(&LOCK);
  t2 = high_resolution_clock::now();
  
  duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
  
  cout << "Duration based on high resolution clock: "<< time_span.count() << " seconds."<<endl; 
  cout << "Number of threads switched in "<<time_span.count() <<" sec: "<<counter<<endl;
  
  /* real apps must check return values */
  ret = sched_rr_get_interval(0, &ts);

  printf("Timeslice: %lu.%lu\n", ts.tv_sec, ts.tv_nsec);

  //quantum
  double q1 = (double)clock() / CLOCKS_PER_SEC;
  //do some calcs
  double i = 0;
  while(i<100000){
    i++;
  }//end while
  double q2 = (double)clock() / CLOCKS_PER_SEC;

  //subtract
  cout<<"Quantum (in ms): "<<(q2-q1)*1000<<endl;
  
  return 0;
}//end main
