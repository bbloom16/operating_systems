/*
 * node.h
 *
 *  Created on: Sep 9, 2018
 *      Author: brian
 */

#ifndef NODE_H_
#define NODE_H_


struct Node{
	int location;
	Node* up;
	Node* down;
	Node* left;
	Node* right;
};

Node* newNode(int location){
	Node* n = new Node;
	n->location = location;
	n->up = n->down = n->left = n-> right = NULL;

	return n;
}//end newNode

//check now many nodes a specific node is touching
int edgeType(Node* nodeIn){
	int edges = 0;
	if(nodeIn->right!=NULL){
		edges++;
	}
	if(nodeIn->left!=NULL){
		edges++;
	}
	if(nodeIn->up!=NULL){
		edges++;
	}
	if(nodeIn->down!=NULL){
		edges++;
	}

	//cout<<"edges: "<<edges<<"\n";
	return edges;

}//end edgeType

enum nodeType {topLeft, topRight, bottomLeft, bottomRight, leftSide, rightSide, topSide, btmSide, middle, problem};


nodeType getNodeType(Node* nodeIn){

	int edges = edgeType(nodeIn);

	//middle
	if(edges==4){
		return middle;
	}
	//side
	else if(edges==3){
		if(nodeIn->up==NULL){
			return topSide;
		}
		else if(nodeIn->down==NULL){
			return btmSide;
		}
		else if(nodeIn->left==NULL){
			return leftSide;
		}
		else if(nodeIn->right==NULL){
			return rightSide;
		}
		else{
			return problem;
		}
	}
	//corner
	else if(edges==2){
		if(nodeIn->up==NULL && nodeIn->left==NULL){
			if(nodeIn->location==0)
				return topLeft;
			else
				return leftSide;
		}
		else if(nodeIn->up==NULL && nodeIn->right==NULL){
			if(nodeIn->location==9)
				return topRight;
			else
				return rightSide;
		}
		else if(nodeIn->down==NULL && nodeIn->left==NULL){
			if(nodeIn->location==90)
				return bottomLeft;
			else
				return leftSide;
		}
		else if(nodeIn->down==NULL && nodeIn->right==NULL){
			if(nodeIn->location==99)
				return bottomRight;
			else
				return rightSide;
		}
		else{
			return problem;
		}
	}
	else{
		return problem;
	}


}//end getNodeType

Node* deleteNodePointers(Node* nodePassed){
	nodePassed->location = -1;
	nodePassed->left = NULL;
	nodePassed->right = NULL;
	nodePassed->up = NULL;
	nodePassed->down = NULL;

	return nodePassed;
}

Node* rowHead[10];

#endif /* NODE_H_ */
