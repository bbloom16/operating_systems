/*
 * main.cpp
 *
 *  Created on: Sep 8, 2018
 *      Author: brian
 */

#include <iostream>
using namespace std;
//for random number
#include <ctime>
#include <cstdlib>
//for node class
#include "node.h"


//run through the pointers
void printGrid(Node* topRight){


	Node* rightPoint;

	for (int i=0; i<10; i++){
		rightPoint = rowHead[i];
		while(rightPoint){
			cout<< rightPoint->location <<"["<<edgeType(rightPoint)<<"]";
			//move to the right
			rightPoint = rightPoint->right;
		}
		cout<< endl;
		//move down
	}

}//end printGrid

//x is length of rows, y is length of cols
Node* makeGridNew(int** gridArray, int x, int y){
//make each row a linked list, link to above rows
	Node *lastPT, *newPT;

	//array of each row's starting pointer (left side)

	for(int i=0; i<x; i++){

		//do each row as linked list (left right pointers)
		for(int j=0; j<y; j++){

				newPT = newNode(gridArray[i][j]);

				//cout<<newPT->location;
				if(j==0){//first in row
					rowHead[i] = newPT;
					//cout<<i<<"\n";
					//cout<<" first in row:"<<rowHead[i]->location;
					lastPT = newPT;

				}
				else if(j==y-1){//last in row
					lastPT->right = newPT;
					newPT->left = lastPT;
				}
				else{//any of the middle positions in the row
					lastPT->right = newPT;
					newPT->left = lastPT;
					lastPT = newPT;
				}
		}//end inner for

	}//end outer for

	//10 linked lists (one per row)
	for(int k=0; k<y; k++){

		Node* thisRow = rowHead[k];
		//cout<<thisRow->location;
		Node *nextRow, *lastRow;

		if(k==0){//top row
			//cout<<"k is zero";
			nextRow = rowHead[k+1];
			while(thisRow!=NULL){
				thisRow->down = nextRow;
				thisRow = thisRow->right;
				nextRow =  nextRow->right;
			}//end while

		}//end if

		else if(k==9){//bottom row
			//cout<<"k is 9";
			lastRow = rowHead[k-1];
			while(thisRow!=NULL){
				thisRow->up = lastRow;

				thisRow = thisRow->right;
				lastRow = lastRow->right;
			}
		}

		else{//any of the middle rows
			//cout<<"k is in the middle";
			nextRow = rowHead[k+1];
			lastRow = rowHead[k-1];
			while(thisRow!=NULL){
				thisRow->up = lastRow;
				thisRow->down = nextRow;
				//advance each row a position
				thisRow = thisRow->right;
				nextRow = nextRow->right;
				lastRow = lastRow->right;
			}
		}

	}//end for

	//return the top left node
	Node* tempy = rowHead[0];

	return tempy;

}//end makeGridTemp

//create a 2D array of size x by y
int** newGrid(int x, int y){
	int** array2D = 0;
	int count = 0;
	array2D = new int*[x];

	for(int a=0; a<x; a++){
		array2D[a] = new int[y];

		for(int b=0; b<y; b++){
			array2D[a][b] = count;
			count = count + 1;
			//cout<<"count "<<count <<" a "<<a<< " b "<<b<< " array "<<array2D[a][b]<<endl;
		}
	}
	return array2D;
}

Node* getByLocation(int locationPassed, Node* pointerGrid){
	Node* rightPoint;



	rightPoint = rowHead[locationPassed/10];
	while(rightPoint){
		if(rightPoint->location==locationPassed)
			return rightPoint;
		//move to the right
		rightPoint = rightPoint->right;
	}

	//cout<<"no point found";
	return NULL;
}//end getByLocation

//when a node is removed, set value to -1
bool isValidNode(Node* nodePassed){

	if(nodePassed!=NULL && nodePassed->location!=-1){
		return true;
	}
	else{
		return false;
	}
}//end isValidNode

Node* deleteNode(Node* pointerGrid){
	srand(time(0));

	int byeNode = (rand() % 100);


	Node* deletionNode = getByLocation(byeNode, pointerGrid);

	//if delete location has already been used, pick another one
	while(isValidNode(deletionNode)==false){
		byeNode = (rand() % 100);
		deletionNode = getByLocation(byeNode, pointerGrid);
		//cout<<"Revised to delete:"<<byeNode;
	}


	cout<<"\n Node to delete: "<<deletionNode->location<<"\n";

	Node *aboveNode= NULL, *belowNode= NULL, *leftNode= NULL, *rightNode = NULL;

	if(deletionNode->up!=NULL){
		aboveNode = deletionNode->up;
	}
	if(deletionNode->down!=NULL){
		belowNode = deletionNode->down;
	}
	if(deletionNode->left!=NULL){
		leftNode = deletionNode->left;
	}
	if(deletionNode->right!=NULL){
		rightNode = deletionNode->right;
	}

	cout<<"deleting node: "<<deletionNode->location<<" type: "<<getNodeType(deletionNode)<<endl;
	switch(getNodeType(deletionNode)){
	case middle:
	        if (aboveNode != NULL) aboveNode->down = belowNode;
		if (belowNode != NULL) belowNode->up = aboveNode;
		if (leftNode != NULL) leftNode->right = rightNode;
		if (rightNode != NULL) rightNode->left = leftNode;
		break;
	case leftSide:
	        if (aboveNode != NULL) aboveNode->down = belowNode;
		if (belowNode != NULL) belowNode->up = aboveNode;
		if (rightNode != NULL) rightNode->left = NULL;
		int k;
		k = (deletionNode->location)/10;
		rowHead[k] = deletionNode->right;
		break;
	case rightSide:
	        if (aboveNode != NULL) aboveNode->down = belowNode;
		if (belowNode != NULL) belowNode->up = aboveNode;
		if (leftNode != NULL) leftNode->right = NULL;
		break;
	case topSide:
	        if (leftNode != NULL) leftNode->right = rightNode;
		if (rightNode !=NULL) rightNode->left = leftNode;
		if (belowNode != NULL) belowNode->up = NULL;
		break;
	case btmSide:
	        if (leftNode != NULL) leftNode->right = rightNode;
		if (rightNode != NULL) rightNode->left = leftNode;
		if (aboveNode != NULL) aboveNode->up = NULL;
		break;
	case topLeft:
	        if (rightNode != NULL) rightNode->left = NULL;
		if (belowNode != NULL) belowNode->up = NULL;
		rowHead[0] = deletionNode->right;
		break;
	case topRight:
	        if (leftNode !=NULL) leftNode->right = NULL;
		if (belowNode != NULL) belowNode->up = NULL;
		break;
	case bottomLeft:
	        if (leftNode != NULL) aboveNode->down = NULL;
		if (rightNode != NULL) rightNode->left = NULL;
		rowHead[9] = deletionNode->right;
		break;
	case bottomRight:
	        if (leftNode != NULL) leftNode->right = NULL;
		if (aboveNode != NULL) aboveNode->down = NULL;
		break;
	case problem:
		cout<<"Problem!";
		break;
	}//end switch

	deletionNode = deleteNodePointers(deletionNode);

	delete deletionNode;
	deletionNode = NULL;

	return rowHead[0];

}//end deleteNode

int main() {

	//make the 2D array (10x10)
	int** gridArray = newGrid(10,10);
	//connect the pointer grid
	Node* pointerGrid = makeGridNew(gridArray, 10, 10);
	//print the grid
	printGrid(pointerGrid);
	Node* newGrid=pointerGrid;

	int i;
	while(newGrid!=NULL){

		//delete a random node
		newGrid = deleteNode(newGrid);
		//print the grid again
		printGrid(newGrid);
		cout<<"\n --------------------- \n";
		//pause loop until input

		//cin>>i;
	}

	return 0;
}//end main



