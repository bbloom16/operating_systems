//brian bloom
#include<iostream>
#include<fstream>
#include<sstream>
#include<sys/types.h>
#include<stdint.h>
#include<string>
#include<stdio.h>
#include<stdlib.h>
#include<crypt.h>
using namespace std;

void readDict(string encryptedPass){
  string fileName = "words.txt";
  ifstream file;
  file.open(fileName.c_str());

  string currentWord;
  while(file >> currentWord){
    string currentEncrypt = string(crypt(currentWord.c_str(), encryptedPass.c_str()));
    cout<<"word: "<<currentWord<<endl;
    //cout<<"currentEncrypt: "<<currentEncrypt<<endl;
    if(currentEncrypt.compare(encryptedPass)==0){
      cout<<"Password: "<<currentWord<<endl;
      exit(0);
    }//end if
  }//end while
  
}//end readDict

int main(){
  string encryptedPass = "$6$XtuNct4U$JK1yl4zBPyqMS2BcuDe1343A4NhWjuM5nedlKysmqp2x9kd2pfhl9n8ycl7XezuDiJGUBxGPnr/5EJovttue./";
  cout<<encryptedPass<<endl;
  readDict(encryptedPass);
  return 0;
}//end main
