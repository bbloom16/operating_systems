#include<iostream>
#include<vector>
using namespace std;

int charFreq [26];
vector<char> codedWord;
vector<char> decodedWord;

void printCompars(){
  cout<<endl;
  for(int i=0; i<codedWord.size(); i+=30){
    cout<<"Decoded:";
    for(int j=i; j<i+30; j++){
      cout<<decodedWord[j];
    }
    cout<<endl;
    cout<<"Encoded:";
    for(int j=i; j<i+30; j++){
      cout<<codedWord[j];
    }
    cout<<endl<<endl;
    
  }
}//end printCompars

void getFreq(string input){  
  //tally frequencies and put all non space chars onto vector in order
  for(int i=0; i<input.size(); i++){
    char temp = input.at(i);
    if(temp!=' '){
      charFreq[temp-97] += 1;
      codedWord.push_back(temp);
      //fill decoded word with all dashes to start
      decodedWord.push_back('-');
    }
  }//end for

  int highestFreq = 0;
  int highestPos = 0;
  //find the highest occuring letter and output all chars and their freq
  for(int i=0; i<26; i++){
    
    int currentFreq = charFreq[i];
    cout<<char(i+97)<<":"<<currentFreq <<endl;
    if(currentFreq>highestFreq){
      highestPos = i;
    }
  }//end for

  //get position of most frequently occuring letter
  cout<<"most frequent character: "<<char(highestPos+97)<<endl;

  //print initial map
  printCompars();

  vector<string> thCombo;
  for(int i=0; i<codedWord.size(); i++){
    if(codedWord[i]==char(highestPos+97)){
      //map most frequent character to letter 'a'
      decodedWord[i] = 'a';
    }
    else if(codedWord[i]=='t'){
      //hardcode map 'i' to the letter 't' based on calcs done on paper
      decodedWord[i] = 'i';
    }
    else if(codedWord[i]=='d'){
      //hardcode map 'm' to the letter 'h' based on calcs done on paper
      decodedWord[i] = 'e';
    }
    else if(codedWord[i]=='k'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 't';
    }
    else if(codedWord[i]=='f'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'h';
    }
    else if(codedWord[i]=='b'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'm';
    }
    else if(codedWord[i]=='m'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 's';
    }
    else if(codedWord[i]=='e'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'c';
    }
    else if(codedWord[i]=='u'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'o';
    }
    else if(codedWord[i]=='p'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'w';
    }
    else if(codedWord[i]=='y'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'l';
    }
    else if(codedWord[i]=='i'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'r';
    }
    else if(codedWord[i]=='o'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'u';
    }
    else if(codedWord[i]=='x'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'd';
    }
    else if(codedWord[i]=='g'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'k';
    }
    else if(codedWord[i]=='r'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'a';
    }
    else if(codedWord[i]=='h'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'n';
    }
    else if(codedWord[i]=='a'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'y';
    }
    else if(codedWord[i]=='c'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'g';
    }
    else if(codedWord[i]=='n'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'p';
    }
    else if(codedWord[i]=='q'){
      //hardcode map 'k' to the letter 'a' based on calcs done on paper
      decodedWord[i] = 'x';
    }
  }

  printCompars();
  
  
}//end getFreq

int main(){
  string input = "kfd ktbd fzm eubd kfd pzyiom mztx ku kzyg ur bzha kfthcm ur mfudm zhx mftnm zhx mdzythc pzq ur ersszcdm zhx gthcm zhx pfa kfd mdz tm sutythc fuk zhx pfdkfdi ntcm fzld pthcm sok pztk z stk kfd uamkdim eitdx sdruid pd fzld uoi efzk rui mubd ur om zid uok ur sidzkf zhx zyy ur om zid rzk hu foiia mztx kfd ezindhkdi kfda kfzhgdx ftb boef rui kfzk";

  //get frequencies of each letter
  getFreq(input);

  return 0;
}//end main
