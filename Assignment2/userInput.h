/*
 * userInput.h
 *
 *  Created on: Oct 9, 2018
 *      Author: brian
 */

#ifndef USERINPUT_H_
#define USERINPUT_H_


struct userInput{
	int argc;
	char ** argv;
};

userInput* newInput(int argc, char ** argv){
	userInput* n = new userInput;
	n->argc = argc;
	n->argv = argv;

	return n;
}//end newNode

void timeOfDay(){
  struct timeval now;
  gettimeofday(&now, NULL);
     cout << "Status report as of : "
          << ctime((time_t *) &now.tv_sec) << endl;
     // Print machine name

     ifstream in ("/proc/sys/kernel/hostname");
     string s;
     in >> s;
     cout << "Machine name: " << s << endl;
     in.close();

}//end timeOfDay

void getPrompt(userInput* inputPassed){
   int argc = inputPassed->argc;
   char ** argv = inputPassed->argv;

   cout<<argv[0]<<endl;
}//end getPrompt

void shellPrompt(){
  std::string userInput = "";
  while(userInput!="exit" || !cin.eof()){
    if(cin.eof()){
      break;
    }//eof on cntrl+d
    cout<<"Shell City>";
    //output shell prompt store user input as args
    string array[20];
    int argc = 1;
    getline(cin, userInput); 
    string sub;
      
      for(int i=0;i<userInput.length();i++){
	if(userInput.length()-1 == i){
	  sub=sub+userInput[i];
	}
	if(userInput[i]==' ' || userInput.length()-1 == i){
	  array[argc-1]=sub;
	  cout<<"SUBSTRING: "<<sub<<endl;
	  argc++;
	  sub="";
	}else{
	   sub=sub+userInput[i];
	}
	//cout<<"i:"<<i<<endl;
      }
      cout<<"argc: "<<argc<<endl;
      //print out the arguments in the form of the array

      char** argv = new char*[argc];

      for(int j=0; j<argc; j++){
	std::string temp = array[j];
	char* cstring = new char [temp.length()+1];
	strcpy (cstring, temp.c_str());

	argv[j] = cstring;
	cout<<"Argv "<<j<<": "<<argv[j]<<endl;
	
      }//end for

      userInput* sendThis = newInput(argc, argv);
      getPrompt(sendThis);
  }//end while
}//end shellPrompt
