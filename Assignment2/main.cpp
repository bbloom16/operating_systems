/*
 * main.cpp
 *
 *  Created on: Sep 8, 2018
 *      Author: brian
 */
#include <ctime>
#include <cstdlib>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <sstream>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <vector>
//for environment variables
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
//for O_RDONLY
#include <fcntl.h>
extern char **environ;
using namespace std;
//for errors
#include <errno.h>
#include <string.h>
extern int errno;

void IORedir(int argc, char ** argv){
  cout<<"IO Redir"<<endl;
  int redirPos = 0;
  bool switchOrder = false;
  char ** newArgs = new char*[100];
  //get characters for redirect and input and output files
  for(int i=0; i<argc; i++){
    if(argv[i]!=NULL && (*argv[i]=='<' || *argv[i]=='>')){
      redirPos = i;
      cout<<"i"<<i<<endl;
    }
  }
  cout<<"Redir pos: "<<redirPos<<endl;

  //assuming write to file is second arg
  int inputFileName;
  int outputFileName;
  if(*argv[redirPos]=='>'){
    cout<<"write to file with >"<<endl;
    cout<<"INPUT FILE: "<<argv[redirPos-1]<<endl;
    inputFileName =open(argv[redirPos-1], O_RDONLY);
    outputFileName = open(argv[redirPos+1],O_WRONLY|O_CREAT,S_IRWXU|S_IRWXG|S_IRWXO);
    cout<<"OUTPUT FILE: "<<argv[redirPos+1]<<endl;
  }//end write to file
  else if(*argv[redirPos]=='<'){
    cout<<"import with <"<<endl;
    //switchOrder = true;
    inputFileName = open(argv[redirPos+1], O_RDONLY);
    outputFileName = open(argv[redirPos-1],O_WRONLY|O_CREAT,S_IRWXU|S_IRWXG|S_IRWXO);

    for(int n=0; n<100; n++){
      newArgs[n] = NULL;
    }
    cout<<"newArgs[0]:"<<argv[redirPos+1]<<endl;
    newArgs[0] = argv[redirPos+1];
    //newArgs[1] = argv[redirPos];
    //newArgs[2] = argv[redirPos-1];

    
  }//end else if

  //cout<<inputFileName<<endl;
  //cout<<outputFileName<<endl;

  dup2(inputFileName, 0);
  dup2(outputFileName, 1);
  close(inputFileName);
  close(outputFileName);
}//end method IORedir

void usePipe(int argc, char ** argv){
  //check to see what position the '|' is in
  int pipePos = 0;
  for(int i=0; i< argc; i++){
    if(argv[i]!=NULL && *argv[i]=='|'){
      pipePos = i;
    }//end if
  }//end for

  string inputFileName = "";
  string outputFileName = "";

  //make two execvp parts
  char * parentArg[100];
  char * childArg[100];
  //initialize both to nulls
  for(int n=0; n<100; n++){
    parentArg[n] = NULL;
    childArg[n] = NULL;
  }

  //do parent arg
  for(int p=0; p<pipePos; p++){
    if(argv[p]!=NULL){
      parentArg[p]=argv[p];
    }
  }//end for

  //print parent arg
  int p1=0;
  while(parentArg[p1]!=NULL){
    //cout<<parentArg[p1]<<endl;
    p1++;
  }
  
  //do childArg
  int childPos = 0;
  for(int c=pipePos+1; c<argc; c++){ 
    if(argv[c]!=NULL){
      childArg[childPos]=argv[c];
      childPos = childPos + 1;
    }
  }//end for

  //print child arg
  int c1=0;
  while(childArg[c1]!=NULL){
    //cout<<childArg[c1]<<endl;
    c1++;
  }
  
  inputFileName = parentArg[0];
  outputFileName = childArg[0];
  
  cout<<"Input file: "<<inputFileName<<endl;
  cout<<"Output file: "<<outputFileName<<endl;

  int pid;
  int status;
  
  
  int thePipe[2];
  pipe(thePipe);

  if((pid = fork()) != -1){
    if(pid == 0){
      close(thePipe[0]);
      close(1);
      dup(thePipe[1]);
      close(thePipe[1]);
      execvp(parentArg[0], parentArg);
    }
    else{
      wait(&status);
      close(thePipe[1]);
      close(0);
      dup(thePipe[0]);
      close(thePipe[0]);
      execvp(childArg[0], childArg);
    }


  }
}//end method usePipe

bool checkPipe(int argc, char ** argv){
  for(int i=0; i<argc; i++){
    if(argv[i]!=NULL && *argv[i] == '|'){
      return true;
    }//end if
  }//end for
  return false;
}//end checkPipe

void getPrompt(int argc, char ** argv){
  bool checkTerm = false;
  bool goIORedir = false;
  bool pipeRedir = checkPipe(argc, argv);
  //check if second argument in argv is &
  if(argv[1]!=NULL && *argv[1]=='&'){
    cout<<"SYMBOL"<<endl;
    checkTerm = true;
  }//end if


  //check for IO redirection 
  for(int m=0; m<argc; m++){
    if(argv[m]!=NULL &&(*argv[m]=='>' || *argv[m]=='<')){
      cout<<"goIr"<<endl;
      goIORedir = true;
    }

  }//end for
  
  ifstream localLook (argv[0]);
  string line = "";
  if(localLook){//file exists either locally or can be found by absolute path
    cout<<"File exists locally"<<endl;
    //CHILD
   
    if(fork() == 0){
      //cout<<"CHILD"<<endl;
      if(goIORedir){
	IORedir(argc, argv);
	execvp(argv[0], argv);
	//	exit(0);
          }
      if(pipeRedir){
	usePipe(argc, argv);
      }//check for pipeRedir
      if(!pipeRedir){
	execvp(argv[0], argv);
      }//no pipeRedir
    }//end if
    //PARENT
    else{
      //cout<<"PARENT"<<endl;
      if(checkTerm==false){
	int status=0;
	wait(&status);
	cout<<"Child exited with status of "<<status<<endl;
      }//end if

    }//end else 
     
  }//end if
  else if(!localLook){//cannot find file locally or by absolute path
    //cout<<"File does not exitst in selected directory...searching PATH"<<endl;
    string PATH = "";
    string temp = "";
    int i = 0;
    while(environ[i]){
      temp = environ[i];
      if(!temp.find("PATH")){
	PATH = temp;
      }//end if
      i++;
    }//end while
    PATH = PATH.substr(5,PATH.size());
    //cout<<PATH<<endl;
    //make path into an array with each path as one string in the array
    std::vector<std::string> array;
    std::stringstream ss(PATH);
    std::string tmp;
    while(std::getline(ss, tmp, ':')){
      //cout<<tmp<<endl;
      array.push_back(tmp);
    }//end while
    //search every directory in the array
    for(int i=0; i<array.size(); i++){
      //cout<<array[i]<<endl;
      const char* find = array[i].c_str();
      char fullPath[100];
      
      string temp = array[i] + '/' + argv[0];
      strcpy(fullPath, temp.c_str());
      
      //cout<<"Full path: "<<fullPath<<endl;
      ifstream pathLook (fullPath);
      string pathLine = "";
      //cout<<pathLook<<endl;
      
      if(pathLook){
	//cout<<"File Found"<<endl;
	//attempt to execute the file
	//CHILD
	if(fork() == 0){
	  //cout<<"CHILD"<<endl;
	  if(goIORedir){
	    cout<<"here"<<endl;
	    IORedir(argc, argv);
	  }//check for IORedir
	  if(pipeRedir){
	    usePipe(argc, argv);
	  }//check for pipe redir
	  if(!pipeRedir || !IORedir){
	    cout<<"no pipe or io redir"<<endl;
	    execvp(fullPath, argv);
	  }//no pipeRedir
	}//end if
	//PARENT
	else{
	  //cout<<"PARENT"<<endl;
	  if(checkTerm==false){
	    int status=0;
	    wait(&status);
	    cout<<"Child exited with status of "<<status<<endl;
	  }//end if
	  
	}//end else
      }//end if      
      pathLook.clear();
      pathLook.close();
    }//end for
  }//end else
  localLook.close();
}//end getArgs

void shellPrompt(){
  std::string userInput = "";
  while(userInput!="exit" || !cin.eof()){
    if(cin.eof()){
      break;
    }//eof on cntrl+d
    cout<<"Shell City>";
    //output shell prompt store user input as args
    string array[20];
    int argc = 1;
    getline(cin, userInput); 
    string sub;
      
      for(int i=0;i<userInput.length();i++){
	if(userInput.length()-1 == i){
	  sub=sub+userInput[i];
	}
	if(userInput[i]==' ' || userInput.length()-1 == i){
	  array[argc-1]=sub;
	  //cout<<"SUBSTRING: "<<sub<<endl;
	  argc++;
	  sub="";
	}else{
	   sub=sub+userInput[i];
	}
	
      }
      //cout<<"argc: "<<argc<<endl;
      //print out the arguments in the form of the array

      char** argv = new char*[argc];

      for(int j=0; j<argc; j++){
	std::string temp = array[j];
	char* cstring = new char [temp.length()+1];
	strcpy (cstring, temp.c_str());
	
	argv[j] = cstring;
	//cout<<"Argv "<<j<<": "<<argv[j]<<endl;
	
      }//end for
      
      argv[argc-1] = NULL;
      getPrompt(argc, argv);
  }//end while
}//end shellPrompt

void shellArt(){
  cout<<"====================================================================================="<<endl;
  cout<<"=      ===============================  ===============      ===  ============  ==  ="<<endl;
  cout<<"=  ===  =============================== ==============  ====  ==  ============  ==  ="<<endl;
  cout<<"=  ====  ============================== ==============  ====  ==  ============  ==  ="<<endl;
  cout<<"=  ===  ===  =   ===  ===   ===  = ========   =========  =======  ======   ===  ==  ="<<endl;
  cout<<"=      ====    =  ======  =  ==     ======  =  ==========  =====    ===  =  ==  ==  ="<<endl;
  cout<<"=  ===  ===  =======  =====  ==  =  =======  ==============  ===  =  ==     ==  ==  ="<<endl;
  cout<<"=  ====  ==  =======  ===    ==  =  ========  ========  ====  ==  =  ==  =====  ==  ="<<endl;
  cout<<"=  ===  ===  =======  ==  =  ==  =  ======  =  =======  ====  ==  =  ==  =  ==  ==  ="<<endl;
  cout<<"=      ====  =======  ===    ==  =  =======   =========      ===  =  ===   ===  ==  ="<<endl;
  cout<<"====================================================================================="<<endl;
  cout<<""<<endl;
}//end shellArt

void shellIntro(){
   struct timeval now;
   gettimeofday(&now, NULL);
   cout<<"Welcome to Brian's Shell"<<endl;
   cout << "Status report as of : "
   << ctime((time_t *) &now.tv_sec) << endl;
   // Print machine name

   ifstream in ("/proc/sys/kernel/hostname");
   string s;
   in >> s;
   cout << "Machine name: " << s << endl;
   in.close();

}//end shellIntro

extern char **environ;
int main() {
  shellArt();
  shellIntro();
  shellPrompt();
	
  return 0;
}//end main
