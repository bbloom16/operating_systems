Part D contains all information needed.  Running Parts C or B will just give info up to the required step.
To get the load average information for Part D, specify by typing ./observer -s [interval] [duration]
To run without this information, type ./observer
In the event you need to re-compile any file, each directory contains a makefile which can be executed using the command 'make'